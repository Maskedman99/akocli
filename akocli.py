'''
Python script to view the deadline dates for all assignments and quizzes of all
subjects in one screen. This can be shown in the terminal/console itself.
'''

# Import the neccessary modules
from requests import get as requests_get, post as requests_post
from json import loads as json_loads
from getpass import getpass
from tabulate import tabulate
from datetime import datetime
import concurrent.futures

# Set up the Moodle server URL and the required endpoints
BASE_URL = 'https://learn.canterbury.ac.nz'
WEB_SERVICE_ENDPOINT = '/webservice/rest/server.php'
LOGIN_ENDPOINT = '/login/token.php'

DATE_FMT = '%d %b %Y %I:%M %p'
DUE_LABELS = ['Due:', 'Closes:', 'Closed:'] 


def get_token():
    """Function to get the token from server."""

    token = None
    while not token:
        # Get the username and password
        username = input("Username: ")
        password = getpass(prompt="Password: ")

        # Set up the request parameters
        params = {
            'username': username,
            'password': password,
            'service': 'moodle_mobile_app'
        }

        # Make the request to the Moodle server
        response = requests_post(BASE_URL + LOGIN_ENDPOINT, params = params)

        if response.status_code == 200:
            data = json_loads(response.text)
            if 'token' in data:
                print("Log in successfull.")
                return data['token'], username   
        
        print("Login failed. Try again. \n")
    
    return None, None


def get_user_id(token, username):
    """Function that returns the userId given the token and username."""

    params = {
        'wstoken': token,
        'wsfunction': 'core_user_get_users_by_field',
        'field': 'email',
        'values[0]': f'{username}@uclive.ac.nz',
        'moodlewsrestformat': 'json'
    }

    response = requests_get(BASE_URL + WEB_SERVICE_ENDPOINT, params = params)

    if response.status_code == 200:
        user_data = json_loads(response.content)
        if user_data:
            print("User id successfully fetched. \n")
            return user_data[0]['id']
    
    print(f"Failed to get the user-id. Is your email {username}@uclive.ac.nz ?")
    return None


def get_course_ids(token, user_id):
    """Function that fetches data from server and returns a dictionary of courses."""
    
    # Initialize the course_dict to an empty dictionary.
    course_dict = dict()

    params = {
        'wstoken': token,
        'wsfunction': 'core_enrol_get_users_courses',
        'userid': user_id,
        'moodlewsrestformat': 'json'
    }

    response = requests_get(BASE_URL + WEB_SERVICE_ENDPOINT, params = params)

    if response.status_code == 200:
        course_data = json_loads(response.content)
        # Iterate over courses and print course names
        for course in course_data:
            course_dict[course['id']] = course['fullname']
    
    if len(course_dict) != 0:
        print("Fetched all the courses you're enrolled in. \n")
    else:
        print("No course found. \n")

    return course_dict

    
def parallel_deadlines(token, course_dict):
    """Function to parallely fetch all the dealines using threads"""

    print("Fetching the deadlines...")
    courses_data = {}

    # add token and course_id into a list, since we can pass only one argument into the function 
    # we call in executer.map()
    arguments = [(token, course_id) for course_id in course_dict.keys()]

    with concurrent.futures.ThreadPoolExecutor() as executor:
        # Map the fetch_deadlines function to each course ID
        futures = executor.map(get_deadlines, arguments)
    
        # Process the results as they become available
        for result in futures:
            course_id, data = result
            if data is not None:
                courses_data[course_id] = data
            else:
                print(f"Something went wrong while fetching data for the course: {course_id}\n")

    print("Fetched the deadlines\n")
    return courses_data


def get_deadlines(arguments):
    """Function to fetch the course contents, parse through it and return the deadlines."""

    # Get the token and course_id from the argument
    token = arguments[0]
    course_id = arguments[1]

    parms = {
        'wstoken': token,
        'wsfunction': 'core_course_get_contents',
        'courseid': course_id,
        'moodlewsrestformat': 'json'
    }

    response = requests_get(BASE_URL + WEB_SERVICE_ENDPOINT, params=parms)

    if response.status_code == 200:
        course_response = json_loads(response.content)

        course_data = {}

        # Iterate over course sections and activities
        for section in course_response:
            section_data = {}
            for activity in section['modules']:
                activity_data = {}
                if 'dates' in activity and activity['dates'] != []:
                    activity_data['name'] = activity['name']
                    activity_data['dates'] = activity['dates']
                    section_data['name'] = section['name']
                    section_data['course'] = course_id

                    if 'activity_datas' not in section_data:
                        section_data['activity_datas'] = []

                    section_data['activity_datas'].append(activity_data)
            
            # If section_data is not empty
            if bool(section_data):
                if 'section_datas' not in course_data:
                    course_data["section_datas"] = []
                
                course_data["section_datas"].append(section_data)
        
        return (course_id, course_data)

    else:
        print(f"Error: {response.status_code} - {response.content}")

    return (course_id, None)


def print_deadlines(courses_data, course_dict):
    """ Function to print the deadlines in a table"""

    table_data = []

    # Prompt user whether they want to see only future dues only
    show_only_future_dues = input("Show only future deadlines ? (y/n): ")

    # Access the deadlines from course_data
    for course_data in courses_data.values():
        for section_datas in course_data.values():
            for section_data in section_datas:
                for activity in section_data['activity_datas']:
                    course_name = course_dict[section_data['course']].split(' ')[0]
                    row = []
                    for date in activity['dates']:
                        # Display only the due dates
                        if date['label'] in DUE_LABELS:
                            # User wants to see only future deadlines
                            if show_only_future_dues == "y" or show_only_future_dues == "Y":
                                if datetime.fromtimestamp(date['timestamp']) > datetime.today():
                                    row.extend([course_name, activity['name']])
                                    row.extend([datetime.fromtimestamp(date['timestamp']).strftime(DATE_FMT)])
                            else:
                                row.extend([course_name, activity['name']])        
                                row.extend([datetime.fromtimestamp(date['timestamp']).strftime(DATE_FMT)])
                    
                    # If row is not empty add it to table_data
                    if len(row) > 0:
                        table_data.append(row)

    # Print the table
    headers = ['Course', 'Activity Name', 'Deadline']
    table = tabulate(table_data, headers, tablefmt='fancy_grid')
    print(table)


def main():
    """ The main function"""

    # Try to login
    token, username = get_token()
        
    # Get the user_id by using the token
    user_id = get_user_id(token, username)
    
    # Proceed only if user_id is not None
    if user_id != None:

        # Get the courses the user is enrolled in by using the user_id
        course_dict = get_course_ids(token, user_id)

        # Proceed only if course_dict is not empty
        if len(course_dict) != 0:

            # Get the deadlines for activities present in each course parallely
            courses_data = parallel_deadlines(token, course_dict)

            # Display the deadlines
            print_deadlines(courses_data, course_dict)


if __name__ == '__main__':
    main()