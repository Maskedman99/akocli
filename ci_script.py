import pyodide.compiler

wasm_bytes = pyodide.compiler.compile_python_to_wasm("akocli.py")
with open("compiled.wasm", "wb") as wasm_file:
    wasm_file.write(wasm_bytes)